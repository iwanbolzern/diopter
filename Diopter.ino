#include "TimerOne.h"
#include "EEPROM_WriteGeneric.h"
#include <EEPROM.h>

const int InterupSequence = 1;
const int MoveDuration = 30;

const int leftIn = 2;
const int rightIn = 3;

const int downIn = 4;
const int upIn = 5;

const int modeOneIn = 6;
const int modeTwoIn = 11;
const int modeThreeIn = 12;
 
const int enMotorHorizontal = 13;
const int motorHorizontalOne = 14;
const int motorHorizontalTwo = 15;

const int enMotorVertical = 13;
const int motorVerticalOne = 14;
const int motorVerticalTwo = 15;

struct config_t
{
    int offsetToZeroH[3];
    int offsetToZeroV[3];
    int activeModule;
} configP;
 
void setup() 
{
  pinMode(leftIn, INPUT);
  pinMode(rightIn, INPUT);
  
  pinMode(downIn, INPUT);
  pinMode(upIn, INPUT);
  
  pinMode(modeOneIn, INPUT);
  pinMode(modeTwoIn, INPUT);
  pinMode(modeThreeIn, INPUT);
  
  pinMode(enMotorHorizontal, OUTPUT);
  pinMode(motorHorizontalOne, OUTPUT);
  pinMode(motorHorizontalTwo, OUTPUT);
  
  pinMode(enMotorVertical, OUTPUT);
  pinMode(motorVerticalOne, OUTPUT);
  pinMode(motorVerticalTwo, OUTPUT);
  
  EEPROM_read(0, configP);
  
  Timer1.initialize(InterupSequence*10000);
  Timer1.attachInterrupt(Move);
} 

int movesRight = 0;
int movesLeft = 0;
int movesDown = 0;
int movesUp = 0;

int activeMoveSiqueles = 0;

void Move() 
{
  if(activeMoveSiqueles == -1) {
    if(movesRight >= 1) {
      movesRight -= 1;
      activeMoveSiqueles = 0;
      digitalWrite(motorHorizontalOne, HIGH);
      digitalWrite(motorHorizontalTwo, LOW);
      digitalWrite(enMotorHorizontal, HIGH);    
    } else if(movesLeft >= 1) {
      movesLeft -= 1;
      activeMoveSiqueles = 0;
      digitalWrite(motorHorizontalOne, LOW);
      digitalWrite(motorHorizontalTwo, HIGH);
      digitalWrite(enMotorHorizontal, HIGH);
    } else if(movesDown >= 1) {
      movesDown -= 1;
      activeMoveSiqueles = 0;
      digitalWrite(motorVerticalTwo, LOW);
      digitalWrite(motorVerticalOne, HIGH);
      digitalWrite(enMotorVertical, HIGH);
    } else if(movesUp >= 1) {
      movesUp -= 1;
      activeMoveSiqueles = 0;
      digitalWrite(motorVerticalTwo, HIGH);
      digitalWrite(motorVerticalOne, LOW);
      digitalWrite(enMotorVertical, HIGH);
    }    
  }
  else if(activeMoveSiqueles >= MoveDuration) {
    digitalWrite(enMotorHorizontal, LOW);
    digitalWrite(enMotorVertical, LOW);
    activeMoveSiqueles = -1;
  } else {
    activeMoveSiqueles += 1;
  }
}

int activeHold = 0;

void loop() 
{ 
  SetActiveHold();
  HandleShifts();
  HandleModes();
  HandleStore();
}

void HandleStore()
{
  if(digitalRead(leftIn) == HIGH && digitalRead(rightIn) == HIGH) {
    //EEPROM_write(0, configP);
    delay(5000);
  }
  else if(digitalRead(downIn) == HIGH && digitalRead(upIn) == HIGH) {
    //EEPROM_write(0, configP);
    delay(5000);
  }
}

void HandleModes()
{
  if(digitalRead(modeOneIn) == LOW && activeHold == modeOneIn && configP.activeModule != 0) 
  {
    CalceOffset(0);
    configP.activeModule = 0;
  } 
  else if(digitalRead(modeTwoIn) == LOW && activeHold == modeTwoIn && configP.activeModule != 1) 
  {
    CalceOffset(1);
    configP.activeModule = 1;
  } 
  else if(digitalRead(modeThreeIn) == LOW && activeHold == modeThreeIn && configP.activeModule != 2) 
  {
    CalceOffset(2);
    configP.activeModule = 2;
  }
}

void CalceOffset(int newMode)
{
  int offsetH = configP.offsetToZeroH[configP.activeModule] - configP.offsetToZeroH[newMode];
  if(offsetH > 0) {
    movesRight += offsetH;
  } 
  else if(offsetH < 0) {
    movesRight += offsetH * -1;
  }
  int offsetV = configP.offsetToZeroV[configP.activeModule] - configP.offsetToZeroV[newMode];
  if(offsetV > 0) {
    movesUp += offsetV;
  } 
  else if(offsetV < 0) {
    movesDown += offsetV * -1;
  }
}

void HandleShifts()
{
  if(digitalRead(leftIn) == LOW && activeHold == leftIn) {
    configP.offsetToZeroH[configP.activeModule] += 1;
    movesRight += 1;
    activeHold = 0;
  }
  else if(digitalRead(rightIn) == LOW && activeHold == rightIn) {
    configP.offsetToZeroH[configP.activeModule] -= 1;
    movesLeft += 1;
    activeHold = 0; 
  }
  else if(digitalRead(downIn) == LOW && activeHold == downIn) {
    configP.offsetToZeroV[configP.activeModule] -= 1;
    movesDown += 1;
    activeHold = 0; 
  }
  else if(digitalRead(upIn) == LOW && activeHold == upIn) {
    configP.offsetToZeroV[configP.activeModule] += 1;
    movesUp += 1;
    activeHold = 0; 
  }
}

void SetActiveHold()
{
  if(digitalRead(leftIn) == HIGH) {
    activeHold = leftIn;
  }
  else if(digitalRead(rightIn) == HIGH) {
    activeHold = rightIn;
  }
  else if(digitalRead(downIn) == HIGH) {
    activeHold = downIn;
  }
  else if(digitalRead(upIn) == HIGH) {
    activeHold = upIn;
  }
  else if(digitalRead(modeOneIn) == HIGH) {
    activeHold = modeOneIn;
  }
  else if(digitalRead(modeTwoIn) == HIGH) {
    activeHold = modeTwoIn;
  }
  else if(digitalRead(modeThreeIn) == HIGH) {
    activeHold = modeThreeIn;
  }
}


